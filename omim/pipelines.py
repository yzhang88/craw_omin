# -*- coding: utf-8 -*-
import sys,os
# sys.setdefaultencoding("utf-8")
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class OmimPipeline(object):
    def process_item(self, item, spider):
        path = os.path.join("result","omim.xls")
        if not os.path.exists("result"):
            os.mkdir("result")
        with open(path,'a') as fh:
            # fh.write(item["Location"]+"\t"+item["Phenotype"]+"\t"+item["Phenotype_MIM_number"]+"\t"+item["Inheritance"]+"\t"+item["Phenotype_mapping_key"]
            #          +"\t"+item["Gene_Locus"]+"\t"+item["Gene_Locus_MIM_number"])
            print '================================================'
            print 'writing in pipeline'
            print '================================================'
            fh.write(item['ctx'])
        return item
