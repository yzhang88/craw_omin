# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class OmimItem(scrapy.Item):
    # define the fields for your item here like:
    # Location = scrapy.Field()
    # Phenotype = scrapy.Field()
    # Phenotype_MIM_number = scrapy.Field()
    # Inheritance = scrapy.Field()
    # Phenotype_mapping_key = scrapy.Field()
    # Gene_Locus = scrapy.Field()
    # Gene_Locus_MIM_number = scrapy.Field()
    ctx = scrapy.Field()
